# PCAN-MiniDisplay trace files batch converter
With the [PEAK-Converter](https://www.peak-system.com/PEAK-Converter.191.0.html?&L=1) you can convert trace files coming from different sources (devices, programs) to target files that can be used for further analyzing.
For batch files or scripting in general, the PEAK-Converter can be called in a command line with parameters. Hence, the user interface isn't shown and files are converted right away without user interaction.

Script converts all *.btr files into *.csv files.

## Installation
Copy `btr_csv_converter.bat` into folder where *.btr files are located.

## Usage
1. start script
2. provide PEAK-Converter.exe working directory

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
