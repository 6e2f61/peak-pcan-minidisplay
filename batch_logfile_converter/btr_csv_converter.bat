@echo off

echo PEAK-Converter command line batch script
set /p peak_converter_path="Setup full PEAK-Converter working directory (without last slash): "
%peak_converter_path%\PEAK-Converter.exe /?
if exist %peak_converter_path%\PEAK-Converter.exe (
	GOTO convert_to_csv
	) else (
	GOTO end_error
	)

:convert_to_csv
echo =============== CSV Converter (*.btr -> *.csv)
set csv_separator=";"
echo =============== CSV Converter starting...
For %%i In ("*.btr") do (
	echo %%i conversion started
	%peak_converter_path%\PEAK-Converter.exe %%i /TF=CSV /CS=%csv_separator%
	echo %%i converted						)
GOTO end_converted

:end_error
echo Conversion failure!
pause
exit

:end_converted
echo End of conversion!
pause
exit