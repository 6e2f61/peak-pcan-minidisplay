# PCAN-MiniDisplay device configuration files
[Device documentation](https://www.peak-system.com/produktcd/Pdf/English/PCAN-MiniDisplay_UserMan_eng.pdf)

**Device Files structure on the Memory Card**

| Directory /MiniDisplay/ | Function |
|---|---|
| Documentation/ | Documentation about the PCAN-MiniDisplay, e.g.this manual |
| Filters/ | A possible place to keep all filter files (*.flt) together |
| Firmware/ | File(s) for updating the firmware. (*.bin) | 
| Help/ | Files with the device help (*.dph) |
| Resources/ | Some bitmaps and fonts as basis for creating own scenes. Furthermore, if you own the PCANExplorer for Windows, you can use the provided example project to generate some data for the PCAN-MiniDisplay scene examples | 
| Scenes/  \<scene name\>/ | A subdirectory with the scene name for each scene. At delivery of the device, the Default scene is set to automatically be shown at startup | 
| Tools/ | Software tools to be used with the PCANMiniDisplay (the following only lists the executables) | 
| Traces/ | Fixed subdirectory for traces that are recorded by the PCAN-MiniDisplay |


List of custom configuration files for PCAN-MiniDisplay device
---

### Filters
- n/a

### Scenes
-  Forsee 1.6 GoEnergy battery 


## Installation
### Filters
- Copy *.flt file into PCAN-MiniDisplay memory card folder `/MiniDisplay/Filters/`

### Scenes
- Copy Forsee-GoBattery folder into PCAN-MiniDisplay memory card folder `/MiniDisplay/Scenes/`

## Usage
### Filters
- Choose Filter from Trace Menu

### Scenes
- Choose Scene from Scenes Menu

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.en.html)
